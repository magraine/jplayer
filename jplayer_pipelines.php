<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


function jplayer_jquery_plugins($plugins) {
	if (_request('page') !== 'login')
	{
		$plugins[] = find_in_path('javascript/jquery.transform2d.js');
		$plugins[] = find_in_path('javascript/jquery.grab.js');
		$plugins[] = find_in_path('javascript/jquery.jplayer.js');
		$plugins[] = find_in_path('javascript/mod.csstransforms.min.js');
		$plugins[] = find_in_path('javascript/circle.player.js');
		$plugins[] = find_in_path('javascript/jplayer.spip.js');
	}
	return $plugins;
}


function jplayer_insert_head($flux) {
	return $flux;
}


function jplayer_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" href="'.find_in_path('css/jplayer.spip.css').'" type="text/css" media="all" />';
	$flux .= '<link rel="stylesheet" href="'.find_in_path('css/jplayer/circle.skin/circle.player.css').'" type="text/css" media="all" />';
	return $flux;
}

