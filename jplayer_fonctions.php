<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Mapper le type attendu par jPlayer en fonction de l'extension du fichier
 * 
 * jPlayer accepte des fichiers différents, mais le type pour le préciser
 * est différent de l'extension par moment. On corrige.
 *
 * @param string $extension
 * @return string
**/
function jplayer_extension_interface($extension) {
	if ($extension == 'ogg') return 'oga';
	return $extension;
}

