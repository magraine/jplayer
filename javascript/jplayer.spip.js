
/**
 * Gestion simple d'une playlist
 *
 * medias : tableau d'objet comme attend jPlayer.
 *
 */
var MM_Playlist = function(medias) {
	var	self = this;

	this.medias = medias;
	this.position  = 0;
	this.count     = this.medias.length;
}

MM_Playlist.prototype = {
	current: function() {
		if (this.count) {
			return this.medias[ this.position ];
		}
		return null;
	},
	next: function() {
		return this.use(this.position + 1);
	},
	prev: function() {
		return this.use(this.position - 1);
	},
	reset: function() {
		return this.use(0);
	},
	end: function() {
		return this.use(this.count - 1);
	},
	isFirst: function() {
		return this.position == 0;
	},
	isLast: function() {
		return this.position == this.count - 1;
	},
	/* i = clé du media a utiliser */
	use: function(i) {
		if (i < 0) return null;
		if (i >= this.count) return null;
		this.position = i;
		return this.current();
	},
	search: function(cle, valeur) {
		for (var i = 0; i < this.count; i++) {
			if (this.medias[i][cle] == valeur) {
				return this.use(i);
			}
		}
		return null;
	}
};
